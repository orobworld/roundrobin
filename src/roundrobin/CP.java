package roundrobin;

import ilog.concert.IloException;
import ilog.concert.IloIntExpr;
import ilog.concert.IloIntVar;
import ilog.cp.IloCP;
import java.util.Arrays;
import java.util.Set;

/**
 * CP uses a constraint programming model to schedule a tournament.
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public final class CP implements AutoCloseable {
  private final Tournament tourney;    // the tournament to schedule
  private final int nGames;            // number of games
  private final int nSlots;            // number of slots
  private final int nPlayers;          // number of players
  private final int nTeams;            // number of teams
  private final int gamesPerSet;       // number of games in a set
  private final IloCP cp;              // the CP model
  private final IloIntVar[] schedule;  // schedule[s] = index of game in slot s

  /**
   * Constructor.
   * @param t the tournament to schedule
   * @throws IloException if CPO won't build the model
   */
  public CP(final Tournament t) throws IloException {
    tourney = t;
    nGames = tourney.getnGames();
    nSlots = tourney.getnSlots();
    nPlayers = tourney.getnPlayers();
    nTeams = tourney.getnTeams();
    gamesPerSet = tourney.getGamesPerSet();
    // Initialize the model.
    cp = new IloCP();
    // Define the variables.
    schedule = new IloIntVar[nSlots];
    for (int s = 0; s < nSlots; s++) {
      schedule[s] = cp.intVar(0, nGames - 1, "slot_" + s);
    }
    // Each game is scheduled exactly once among the first 45 games.
    IloIntVar[] temp = Arrays.copyOfRange(schedule, 0, nGames);
    cp.add(cp.allDiff(temp));
    // Each game is scheduled at most twice.
    IloIntVar[] cards = new IloIntVar[nGames];
    for (int g = 0; g < nGames; g++) {
      cards[g] = cp.intVar(1, 2);
    }
    cp.add(cp.distribute(cards, schedule));
    // Each player plays exactly four games from each set.
    int pgps = tourney.getGamesPlayedPerSet();   // games per set for one player
    for (int p = 0; p < nPlayers; p++) {
      Set<Integer> games = tourney.gamesPlayedIn(p);  // games the player is in
      for (int s : tourney.getStartingSlots()) {
        IloIntVar[] set = getSet(s);
        IloIntExpr expr = cp.constant(0);
        for (int g : games) {
          expr = cp.sum(expr, cp.count(set, g));
        }
        cp.addEq(expr, pgps);
      }
    }
    // No player sits in consective games withing a set.
    for (int p = 0; p < nPlayers; p++) {
      Set<Integer> games = tourney.gamesPlayedIn(p);  // games the player is in
      for (int s : tourney.getFollowedSlots()) {
        IloIntExpr expr = cp.constant(0);
        for (int g : games) {
          expr = cp.sum(expr, cp.eq(schedule[s], g));
          expr = cp.sum(expr, cp.eq(schedule[s + 1], g));
        }
        cp.addGe(expr, 1);
      }
    }
    // No team plays more than once in a set.
    for (int team = 0; team < nTeams; team++) {
      // Get the games in which the team plays as an int vector.
      Set<Integer> games = tourney.gamesTeamIn(team);
      // For each set, count the number of games containing the team.
      for (int s : tourney.getStartingSlots()) {
        IloIntVar[] set = getSet(s);
        IloIntExpr expr = cp.constant(0);
        for (int g : games) {
          expr = cp.sum(expr, cp.count(set, g));
        }
        cp.addLe(expr, 1);
      }
    }
    // Antisymmetry constraint: since any permutation of player IDs in a
    // feasible schedule results in another feasible schedule, we can require
    // that the first game is scheduled in the first slot.
    cp.addEq(schedule[0], 0);
    // New constraint: Every team must play at least once per day.
    int nDays = tourney.getnDays();
    int slotsPerDay = tourney.getnSlotsPerDay();
    for (int team = 0; team < nTeams; team++) {
      Set<Integer> games = tourney.gamesTeamIn(team);
      for (int day = 0; day < nDays; day++) {
        int firstSlot = slotsPerDay * day;
        IloIntVar[] slots =
          Arrays.copyOfRange(schedule, firstSlot, firstSlot + slotsPerDay);
        IloIntExpr expr = cp.constant(0);
        for (int g : games) {
          expr = cp.sum(expr, cp.count(slots, g));
        }
        cp.addGe(expr, 1);
      }
    }
    // New constraint: limit the number of times in any one set that any
    // pair of players play against each other.
    int maxOpposed = tourney.getMaxOpposed();
    for (int team = 0; team < nTeams; team++) {
      Set<Integer> games = tourney.gamesOpposed(team);
      // Generate a constraint for each set.
      for (int s : tourney.getStartingSlots()) {
        IloIntVar[] slots =
          Arrays.copyOfRange(schedule, s, s + gamesPerSet);
        IloIntExpr expr = cp.constant(0);
        for (int g : games) {
          expr = cp.sum(expr, cp.count(slots, g));
        }
        cp.addLe(expr, maxOpposed);
      }
    }
  }

  /**
   * Solves the IP model.
   * @param timeLimit the time limit (in seconds)
   * @return true if a solution was found
   * @throws IloException if CPLEX blows up
   */
  public boolean solve(final double timeLimit) throws IloException {
    cp.setParameter(IloCP.DoubleParam.TimeLimit, timeLimit);
    return cp.solve();
  }

  /**
   * Gets the solution.
   * @return an array of game indices in the order played
   * @throws IloException if the solution does not exist
   */
  public int[] getSolution() throws IloException {
    int[] sol = new int[nSlots];
    for (int s = 0; s < nSlots; s++) {
      sol[s] = (int) cp.getValue(schedule[s]);
    }
    return sol;
  }

  /**
   * Closes the model.
   */
  @Override
  public void close() {
    cp.end();
  }

  /**
   * Gets the schedule variables corresponding to a specific set.
   * @param firstSlot the first slot in the set
   * @return the schedule variables for that set
   */
  private IloIntVar[] getSet(final int firstSlot) {
    return Arrays.copyOfRange(schedule, firstSlot, firstSlot + gamesPerSet);
  }
}
