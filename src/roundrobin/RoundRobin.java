package roundrobin;

import ilog.concert.IloException;
import ilog.cplex.IloCplex;

/**
 * RoundRobin solves a tournament scheduling problem posed on OR Stack Exchange.
 *
 * Source:
 * https://or.stackexchange.com/questions/9545/doubles-round-robin-sorting
 * -algorithm
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public final class RoundRobin {

  /**
   * Dummy constructor.
   */
  private RoundRobin() { }

  /**
   * Sets up and solves the model.
   * @param args the command line arguments (ignored)
   */
  @SuppressWarnings({ "checkstyle:magicnumber" })
  public static void main(final String[] args) {
    // Create the tournament object.
    Tournament tourney = new Tournament();
    // Try solving the IP model.
    System.out.println("Using the integer programming model ...");
    try (IP ip = new IP(tourney)) {
      double timeLimit = 60;
      IloCplex.Status status = ip.solve(timeLimit);
      if (status == IloCplex.Status.Optimal) {
        int[] solution = ip.getSolution();
        tourney.printSchedule(solution);
      } else {
        System.out.println("Final solver status = " + status);
      }
    } catch (IloException ex) {
      System.out.println(ex.getMessage());
    }
    // Try solving the CP model.
    System.out.println("\nUsing the constraint programming model ...");
    try (CP cp = new CP(tourney)) {
      double timeLimit = 30;
      if (cp.solve(timeLimit)) {
        int[] solution = cp.getSolution();
        tourney.printSchedule(solution);
      } else {
        System.out.println("No solution found!");
      }
    } catch (IloException ex) {
      System.out.println(ex.getMessage());
    }
  }

}
