package roundrobin;

/**
 * Team records a pair of players, either playing together or sitting out
 * together.
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public record Team(int firstPlayer, int secondPlayer, int teamNumber) {
  private static final String[] names =
    new String[] {"A", "B", "C", "D", "E", "F"};

  /**
   * Constructor.
   * @param firstPlayer the index of the first player
   * @param secondPlayer the index of the second player
   */
  public Team(int firstPlayer, int secondPlayer, int teamNumber) {
    // Make sure the players are in alphabetic order.
    this.firstPlayer = Math.min(firstPlayer, secondPlayer);
    this.secondPlayer = Math.max(firstPlayer, secondPlayer);
    this.teamNumber = teamNumber;
  }

  /**
   * Provides a string containing the player names.
   * @return a string of two characters indicating the players
   */
  @Override
  public String toString() {
    return names[firstPlayer] + names[secondPlayer];
  }

  /**
   * Tests whether a player is on the team.
   * @param player the player in question
   * @return true if the player is on the team
   */
  public boolean contains(final int player) {
    return (firstPlayer == player) || (secondPlayer == player);
  }

  /**
   * Tests if this team is disjoint from another team (no players in common).
   * @param other the other team
   * @return true if the two teams do not share a player
   */
  public boolean isDisjointFrom(final Team other) {
    return !(other.contains(firstPlayer) || other.contains(secondPlayer));
  }
}
