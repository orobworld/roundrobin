package roundrobin;

/**
 * Game represents one possible game.
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public record Game(Team first, Team second, Team bye, int index) {
  /**
   * Produces a string summarizing the game.
   * @return a string representation of the game
   */
  @Override
  public String toString() {
    return first + " vs. " + second + " (bye: " + bye + ")";
  }
}
