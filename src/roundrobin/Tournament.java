package roundrobin;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.IntStream;

/**
 * Tournament contains details of the tournament.
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public final class Tournament {
  // Dimensions.
  private final int nPlayers = 6;               // number of players
  private final int nTeams = 15;                // number of distinct teams
  private final int nGames = 45;                // number of games to play
  private final int nSlots = 48;                // game slots to fill
  private final int nSets = 8;                  // number of sets of games
  private final int gamesPerSet = 6;            // games per set
  private final int setsPerDay = 2;             // sets per day
  private final int days = 4;                   // days the tournament runs
  private final int gamesPlayedPerSet = 4;      // games per set for one player
  private final int maxOpposed = 2;             // max number of games per set
                                                // with same two players on
                                                // opposite sides

  private final Set<Integer> players;           // set of player IDs
  private final Set<Integer> teamIndices;       // set of team indices
  private final Team[] teams;                   // array of all teams
  private final Game[] games;                   // vector of all possible games
  private final int[] startingSlots;            // slots starting a set
  private final int[] followedSlots;            // slots followed by a slot
  private final Map<Integer, Set<Integer>> belongsTo;
    // maps players to indices of teams to which they belong
  private final Map<Integer, Set<Integer>> playerIn;
    // maps players to indices of games they play in
  private final Map<Integer, Set<Integer>> teamIn;
    // maps teams to indices of games they play in
  private final Map<Integer, Set<Integer>> gamesOpposed;
    // maps "teams" (pairs of players) to indices of games in which they
    // play on opposite sides

  /**
   * Constructor.
   */
  public Tournament() {
    // Initialize the maps.
    players = new HashSet<>();
    belongsTo = new HashMap<>();
    playerIn = new HashMap<>();
    teamIn = new HashMap<>();
    teamIndices = new HashSet<>();
    gamesOpposed = new HashMap<>();
    for (int p = 0; p < nPlayers; p++) {
      players.add(p);
      belongsTo.put(p, new HashSet<>());
      playerIn.put(p, new HashSet<>());
    }
    for (int t = 0; t < nTeams; t++) {
      teamIndices.add(t);
      teamIn.put(t, new HashSet<>());
      gamesOpposed.put(t, new HashSet<>());
    }
    // Generate the list of teams.
    teams = new Team[nTeams];
    buildAllTeams();
    // Generate the list of all possible games.
    games = new Game[nGames];
    buildAllGames();
    // Compute the starting slots.
    startingSlots =
      IntStream.range(0, nSets).map(i -> i * gamesPerSet).toArray();
    // Compute the slots that are followed by another slot in the same set.
    followedSlots =
      IntStream.range(0, nSlots).filter(i -> ((i + 1) % gamesPerSet != 0))
               .toArray();
    // Compute for each pair of players (team) the set of games in which
    // they play against each other.
    for (int t = 0; t < nTeams; t++) {
      int p1 = teams[t].firstPlayer();
      int p2 = teams[t].secondPlayer();
      // Find games in which both players play.
      Set<Integer> both = new HashSet<>(playerIn.get(p1));
      both.retainAll(playerIn.get(p2));
      // Drop games where they play togther.
      both.removeAll(teamIn.get(t));
      gamesOpposed.put(t, both);
    }
  }

  /**
   * Builds the array of all possible teams.
   */
  private void buildAllTeams() {
    int next = 0;
    for (int p1 = 0; p1 < nPlayers; p1++) {
      for (int p2 = p1 + 1; p2 < nPlayers; p2++) {
        teams[next] = new Team(p1, p2, next);
        belongsTo.get(p1).add(next);
        belongsTo.get(p2).add(next);
        next += 1;
      }
    }
  }

  /**
   * Builds the array of all possible games.
   *
   * In each game, two teams play against each other while a third "team" has
   * a bye. To avoid duplication, the first team playing must
   * have lower index than the second playing team. The three teams must
   * be disjoint.
   */
  private void buildAllGames() {
    int next = 0;  // index of game to build
    for (int t1 = 0; t1 < nTeams; t1++) {
      Team team1 = teams[t1];
      for (int t2 = t1 + 1; t2 < nTeams; t2++) {
        Team team2 = teams[t2];
        if (team2.isDisjointFrom(team1)) {
          Team team3 = teams[byeTeam(t1, t2)];
          games[next] = new Game(team1, team2, team3, next);
          // Mark the first two teams as playing in the game.
          teamIn.get(t1).add(next);
          teamIn.get(t2).add(next);
          // Mark the players in the first two teams as playing in the game.
          Team team = teams[t1];
          playerIn.get(team.firstPlayer()).add(next);
          playerIn.get(team.secondPlayer()).add(next);
          team = teams[t2];
          playerIn.get(team.firstPlayer()).add(next);
          playerIn.get(team.secondPlayer()).add(next);
          next += 1;
        }
      }
    }

  }

  /**
   * Finds the index of the "team" with a bye when two teams play.
   * @param t1 the index of the first playing team
   * @param t2 the index of the second playing team
   * @return the index of the "bye team"
   */
  private int byeTeam(final int t1, final int t2) {
    // Find the two unused players.
    HashSet<Integer> unused = new HashSet<>(players);
    unused.remove(teams[t1].firstPlayer());
    unused.remove(teams[t1].secondPlayer());
    unused.remove(teams[t2].firstPlayer());
    unused.remove(teams[t2].secondPlayer());
    HashSet<Integer> t = new HashSet<>(teamIndices);
    for (int p : unused) {
      t.retainAll(belongsTo.get(p));
    }
    // There should be one team left in t.
    return t.iterator().next();
  }

  /**
   * Gets the number of games.
   * @return the number of games
   */
  public int getnGames() {
    return nGames;
  }

  /**
   * Gets the number of slots.
   * @return the number of slots
   */
  public int getnSlots() {
    return nSlots;
  }

  /**
   * Gets the number of players.
   * @return the player count
   */
  public int getnPlayers() {
    return nPlayers;
  }

  /**
   * Gets the number of teams.
   * @return the number of teams
   */
  public int getnTeams() {
    return nTeams;
  }

  /**
   * Gets the number of days in the tournament.
   * @return the number of days
   */
  public int getnDays() {
    return days;
  }

  /**
   * Gets the number of slots per day.
   * @return the number of slots per day.
   */
  public int getnSlotsPerDay() {
    return setsPerDay * gamesPerSet;
  }

  /**
   * Gets the number of games per set.
   * @return the number of games per set.
   */
  public int getGamesPerSet() {
    return gamesPerSet;
  }

  /**
   * Gets the number of games any one player plays in a single set.
   * @return the games played per set for one player
   */
  public int getGamesPlayedPerSet() {
    return gamesPlayedPerSet;
  }

  /**
   * Gets the set of indices of games in which a player appears.
   * @param player the player
   * @return the set of (indices of) games played in
   */
  public Set<Integer> gamesPlayedIn(final int player) {
    return Collections.unmodifiableSet(playerIn.get(player));
  }

  /**
   * Gets the array of starting slots for game sets.
   * @return the array of starting slots
   */
  public int[] getStartingSlots() {
    return Arrays.copyOf(startingSlots, startingSlots.length);
  }

  /**
   * Gets the array of slots that are followed immediately (same set) by
   * other slots.
   * @return the array of followed slots
   */
  public int[] getFollowedSlots() {
    return Arrays.copyOf(followedSlots, followedSlots.length);
  }

  /**
   * Gets the set of indices of games containing a team.
   * @param team the index of the team
   * @return the set of indices of the team's games
   */
  public Set<Integer> gamesTeamIn(final int team) {
    return Collections.unmodifiableSet(teamIn.get(team));
  }

  /**
   * Gets the set of indices of games in which two players are opposed.
   * @param team the team containing the two players
   * @return the set of indices of games where they are on opposing teams
   */
  public Set<Integer> gamesOpposed(final int team) {
    return Collections.unmodifiableSet(gamesOpposed.get(team));
  }

  /**
   * Gets the maximum number of games per set in which the same two players
   * face each other.
   * @return the maximum times two players can be opposed in one set
   */
  public int getMaxOpposed() {
    return maxOpposed;
  }

  /**
   * Prints a schedule of games.
   * @param schedule an array of indices of games in the order played.
   */
  public void printSchedule(final int[] schedule) {
    System.out.println("\nTournament Schedule\n");
    int next = 0;
    for (int d = 0; d < days; d++) {
      for (int s = 0; s < setsPerDay; s++) {
        System.out.println("Day " + (d + 1) + ", Set " + (s + 1) + ": ");
        for (int g = 0; g < gamesPerSet; g++) {
          System.out.println("\t" + games[schedule[next]]);
          next += 1;
        }
      }
      System.out.println();
    }
  }
}
