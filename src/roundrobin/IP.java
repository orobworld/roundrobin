package roundrobin;

import ilog.concert.IloException;
import ilog.concert.IloLinearNumExpr;
import ilog.concert.IloNumVar;
import ilog.cplex.IloCplex;
import java.util.Arrays;
import java.util.Set;

/**
 * IP uses an integer programming model to schedule a tournament.
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public final class IP implements AutoCloseable {
  private static final double HALF = 0.5;  // for rounding
  private final Tournament tourney;   // the tournament to schedule
  private final int nGames;           // number of games
  private final int nSlots;           // number of slots
  private final int nPlayers;         // number of players
  private final int nTeams;           // number of teams
  private final IloCplex ip;          // the IP model
  private final IloNumVar[][] x;      // the scheduling variables

  /**
   * Constructor.
   * @param t the tournament to schedule
   * @throws IloException if CPLEX won't build the model
   */
  public IP(final Tournament t) throws IloException {
    tourney = t;
    nGames = tourney.getnGames();
    nSlots = tourney.getnSlots();
    nPlayers = tourney.getnPlayers();
    nTeams = tourney.getnTeams();
    // Initialize the model.
    ip = new IloCplex();
    // Define the variables.
    x = new IloNumVar[nGames][nSlots];
    for (int g = 0; g < nGames; g++) {
      for (int s = 0; s < nSlots; s++) {
        x[g][s] = ip.boolVar("game_" + g + "_slot_" + s);
      }
    }
    // Each game is scheduled exactly once among the first 45 games.
    for (int g = 0; g < nGames; g++) {
      IloNumVar[] temp = Arrays.copyOfRange(x[g], 0, nGames);
      ip.addEq(ip.sum(temp), 1.0);
    }
    // Each game is scheduled at least once and at most twice.
    for (int g = 0; g < nGames; g++) {
      ip.addLe(ip.sum(x[g]), 2.0);
    }
    // Each slot is filled exactly once.
    for (int s = 0; s < nSlots; s++) {
      IloLinearNumExpr expr = ip.linearNumExpr();
      for (int g = 0; g < nGames; g++) {
        expr.addTerm(1.0, x[g][s]);
      }
      ip.addEq(expr, 1.0);
    }
    // Each player plays exactly four games from each set.
    int gamesPerSet = tourney.getGamesPerSet();  // total games in a set
    int pgps = tourney.getGamesPlayedPerSet();   // games per set for one player
    for (int p = 0; p < nPlayers; p++) {
      Set<Integer> games = tourney.gamesPlayedIn(p);  // games the player is in
      for (int s : tourney.getStartingSlots()) {
        IloLinearNumExpr expr = ip.linearNumExpr();
        for (int s0 = s; s0 < s + gamesPerSet; s0++) {
          for (int g : games) {
            expr.addTerm(1.0, x[g][s0]);
          }
        }
        ip.addEq(expr, pgps);
      }
    }
    // No player sits in consective games withing a set.
    for (int p = 0; p < nPlayers; p++) {
      for (int s : tourney.getFollowedSlots()) {
        IloLinearNumExpr expr = ip.linearNumExpr();
        for (int g : tourney.gamesPlayedIn(p)) {
          expr.addTerm(1.0, x[g][s]);
          expr.addTerm(1.0, x[g][s + 1]);
        }
        ip.addGe(expr, 1.0);
      }
    }
    // No team plays more than once in a set.
    for (int team = 0; team < nTeams; team++) {
      Set<Integer> games = tourney.gamesTeamIn(team);
      for (int s : tourney.getStartingSlots()) {
        IloLinearNumExpr expr = ip.linearNumExpr();
        for (int s0 = s; s0 < s + gamesPerSet; s0++) {
          for (int g : games) {
            expr.addTerm(1.0, x[g][s0]);
          }
        }
        ip.addLe(expr, 1.0);
      }
    }
    // Antisymmetry constraint: since any permutation of player IDs in a
    // feasible schedule results in another feasible schedule, we can require
    // that the first game is scheduled in the first slot.
    x[0][0].setLB(1);
    // New constraint: Every team must play at least once per day.
    int nDays = tourney.getnDays();
    int slotsPerDay = tourney.getnSlotsPerDay();
    for (int team = 0; team < nTeams; team++) {
      Set<Integer> games = tourney.gamesTeamIn(team);
      for (int day = 0; day < nDays; day++) {
        int firstSlot = slotsPerDay * day;
        IloLinearNumExpr expr = ip.linearNumExpr();
        for (int slot = firstSlot; slot < firstSlot + slotsPerDay; slot++) {
          for (int g : games) {
            expr.addTerm(1.0, x[g][slot]);
          }
        }
        ip.addGe(expr, 1.0, "team_" + team + "_day_" + day);
      }
    }
    // New constraint: limit the number of times in any one set that any
    // pair of players play against each other.
    int maxOpposed = tourney.getMaxOpposed();
    for (int team = 0; team < nTeams; team++) {
      Set<Integer> games = tourney.gamesOpposed(team);
      // Generate a constraint for each set.
      for (int s : tourney.getStartingSlots()) {
        IloLinearNumExpr expr = ip.linearNumExpr();
        for (int s0 = s; s0 < s + gamesPerSet; s0++) {
          for (int g : games) {
            expr.addTerm(1.0, x[g][s0]);
          }
        }
        ip.addLe(expr, maxOpposed);
      }
    }
  }

  /**
   * Solves the IP model.
   * @param timeLimit the time limit (in seconds)
   * @return the final solver status
   * @throws IloException if CPLEX blows up
   */
  public IloCplex.Status solve(final double timeLimit) throws IloException {
    ip.setParam(IloCplex.DoubleParam.TimeLimit, timeLimit);
    ip.solve();
    return ip.getStatus();
  }

  /**
   * Gets the solution.
   * @return an array of game indices in the order played
   * @throws IloException if the solution does not exist
   */
  public int[] getSolution() throws IloException {
    int[] sol = new int[nSlots];
    for (int s = 0; s < nSlots; s++) {
      for (int g = 0; g < nGames; g++) {
        if (ip.getValue(x[g][s]) > HALF) {
          sol[s] = g;
          break;
        }
      }
    }
    return sol;
  }

  /**
   * Closes the model.
   */
  @Override
  public void close() {
    ip.close();
  }

}
