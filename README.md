# Round Robin #

### What is this repository for? ###

This code uses a binary integer programming (IP) model to schedule a round robin tournament as described in a [question](https://or.stackexchange.com/questions/9545/doubles-round-robin-sorting-algorithm) on Operations Research Stack Exchange.

There is no criterion function mentioned in the original question; the author only seeks a feasible solution. The model here uses the default objective function (minimize 0).

### Details ###

The code was developed in Java using CPLEX 22.1 but should run with any version. CPLEX is overkill for a problem this size. Any MIP solver would do.

The model formulation is posted in my [answer](https://or.stackexchange.com/a/9548/67).

### License ###
The code here is copyrighted by Paul A. Rubin (yours truly) and licensed under a [Creative Commons Attribution 3.0 Unported License](http://creativecommons.org/licenses/by/3.0/deed.en_US). 

